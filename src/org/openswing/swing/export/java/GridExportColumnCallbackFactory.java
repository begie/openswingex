package org.openswing.swing.export.java;

import org.openswing.swing.table.columns.client.Column;
import org.openswing.swing.table.columns.client.ComboColumn;


/**
 * <p>Title: OpenSwing Framework</p>
 * <p>Description: creates classes implementing 
 * GridExportColumnCallback based on column type.</p>
 * <p>Copyright: Copyright (C) 2013 Bartlomiej Gola</p>
 *
 * <p> This file is part of OpenSwing Framework.
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the (LGPL) Lesser General Public
 * License as published by the Free Software Foundation;
 *
 *                GNU LESSER GENERAL PUBLIC LICENSE
 *                 Version 2.1, February 1999
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *       The author may be contacted at:
 *           gola.bartlomiej@gmail.com</p>
 *
 * @author Bartlomiej Gola
 * @version 1.0
 */
public class GridExportColumnCallbackFactory {

  private static GridExportColumnCallbackFactory instance = null;
  
  public static GridExportColumnCallbackFactory getInstance() {
    if (instance == null) {
      instance = new GridExportColumnCallbackFactory();
    }
    return instance;
  }

  protected static void setInstance(GridExportColumnCallbackFactory instance) {
    GridExportColumnCallbackFactory.instance = instance;
  }

  public GridExportColumnCallback create(Column col) {
    if (col instanceof ComboColumn) {
      return new GridExportComboColumnCallback((ComboColumn) col);
    }
    return null;
  }
  
}